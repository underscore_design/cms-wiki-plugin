<?php
/*
Plugin Name: CMS Wiki
Plugin URI: http://www.underscore.co.uk
Description: Easy building and managing documentation for user. Plugin requires ACF with repeater extension.
Author: Martin Jarcik / Underscore.co.uk
Version: 0.1
Author URI: http://www.underscore.co.uk

==================================================

DESCRIPTION:
Plugin in cooperation with ACF creates custom post type for managing simple wiki in administration. Original admin ID=1 has ability to edit post using ACF Repeater. Other users see administration without all editable elemens.

==================================================

ROOM TO IMPROVE:
Hide attachements from Wiki in media library

==================================================

SETUP ACF:
----------

	Create Field Group: CMS Wiki
		+ Page = Repeater
			+ Title = Text
			+ Content = Wysiwyg

	Layout: Row

	Location:
		+ Post Type is equal to CMS Wiki
		[AND] Logged in User Type is equal to Administrator

	Hide on screen: All

*/


if (!class_exists('cms_wiki')) {
	
	class cms_wiki {

		public static function initialize() {
					
			// Register post type
			add_action( 'init', function() {
	
				$create_posts_cap = ( get_current_user_id() == 1 ? true : false );

				register_post_type( 'cms_wiki',
					array(
						'labels' => array(
							'name'				=> 'Documentation',
							'singular_name'		=> 'documentation',
							'menu_name'         => 'Documentation'
						),

					'menu_icon' => 'dashicons-book',	
					'public'					=> true,
					'has_archive' 				=> false,
					'exclude_from_search' 		=> 'true',
					'supports' 					=> array('title', 'custom-fields'),
					'capability_type' 			=> 'post',
					'capabilities' 				=> array('create_posts' => $create_posts_cap),
					'map_meta_cap' => true
					)
				); 
				
			});


			// Remove quick edit menu in list page
			add_filter( 'post_row_actions', function ( $actions, $post) {
				
				if( $post->post_type == 'cms_wiki' ) return array();
				else return $actions;

			}, 10, 2 );


			// Romeve columns in list page
			add_filter( 'manage_edit-cms_wiki_columns',  function(){ return array( 'title' => 'Name' ); }) ;

			add_filter( 'bulk_actions-edit-cms_wiki', '__return_empty_array');

			// Remove save box in detail
			add_action( 'admin_menu', function () {

			    if (get_current_user_id() != 1) remove_meta_box( 'submitdiv', 'cms_wiki', 'side' );
			    
			    remove_meta_box( 'slugdiv', 'cms_wiki', 'normal' ); 

			});

			add_action( 'add_meta_boxes', function () {
			    remove_meta_box( 'slugdiv', 'cms_wiki', 'normal' );
			});
						
			// Show custom content in detail
			add_action( 'edit_form_after_title', array('cms_wiki', 'generate_detail_page') );
			

			// Force 1 column layout on edit page
			add_filter( 'get_user_option_screen_layout_cms_wiki', function() { return 1; } );

			// Add custom CSS + JS to head
			add_action('admin_head', array('cms_wiki', 'admin_head') );


			// Hide uploaded images from regular administration. 
			add_action("add_attachment", function ($attachment_ID){   
				
				$post = get_post($attachment_ID);

				if($post->post_parent) {
					$parent = get_post($post->post_parent);
				}

				if ($parent->post_type == 'cms_wiki') update_post_meta($attachment_ID, "cms_wiki", 1);
			    
			});


			add_action('pre_get_posts', function ($query) {

				$do_filter = false;

				if (is_admin()) {					

					if (isset($_POST['post_id'])) {
						$post = get_post($_POST['post_id']);
						if ($post->post_type != 'cms_wiki') $do_filter = true;
					}

					$screen = get_current_screen();
					if ( isset($screen) && $screen->base == 'upload') $do_filter = true;

					if ( $do_filter) {
						$meta_query[] = array( 'key'=>'cms_wiki', 'compare'=>'NOT EXISTS' );
						$query->set('meta_query',$meta_query);		
					}
				}	

			});			

			

		} // #initialize

		public static function admin_head() {
			
			$screen = get_current_screen();

			if( $screen->post_type == 'cms_wiki' ) :
			?>

				<style>

					#edit-slug-box,
					#posts-filter .tablenav,
					#posts-filter .search-box,
					#wpbody-content .subsubsub { display: none; }


				</style>

				<script>
				</script>

			<?php
			endif;
		}


		public static function generate_detail_page() {

			if( !get_field('page') || get_current_user_id() == 1) return;

			echo '<div class="postbox">';

			while( has_sub_field('page') ) {

				echo '<h3>'.get_sub_field('title').'</h3>';
				echo '<div class="inside">'.get_sub_field('content').'</div>';

			}

			echo '</div>';


		}  // #generate_detail_page

	}

	if (class_exists('acf')) cms_wiki::initialize();

}

?>